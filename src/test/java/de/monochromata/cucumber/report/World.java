package de.monochromata.cucumber.report;

import java.nio.file.Path;

public class World {

    public Path featureFile;
    public Path testOutputDir;
    public Path propertiesFile;
    public Path trendsStatsFile;

}
