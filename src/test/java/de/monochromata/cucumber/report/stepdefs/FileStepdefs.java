package de.monochromata.cucumber.report.stepdefs;

import static de.monochromata.cucumber.report.config.ConfigurationFactory.CONFIG_FILE_PROPERTY;
import static java.lang.System.lineSeparator;
import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.Files.createTempFile;
import static java.nio.file.Files.lines;
import static java.nio.file.Files.write;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Paths;

import cucumber.api.java8.En;
import de.monochromata.cucumber.report.World;

public class FileStepdefs implements En {

    private boolean defaultPropertiesFileCreated = false;
    private boolean customPropertiesFileCreated = false;
    private String originalTrendsStats;

    public FileStepdefs(final World world) {
        Given("test output directory $testOutputDir", () -> {
            world.testOutputDir = createTempDirectory("testOutputDir");
            world.testOutputDir.toFile().deleteOnExit();
        });

        Given("properties file in default location containing", (final String contentsWithPlaceholders) -> {
            world.propertiesFile = Paths.get("cucumber-reporting.properties");
        	writePropertiesFile(world, contentsWithPlaceholders);
            defaultPropertiesFileCreated = true;
        });

        Given("properties file $propertiesFile containing", (final String contentsWithPlaceholders) -> {
            world.propertiesFile = createTempFile("propertiesFile", ".properties");
            writePropertiesFile(world, contentsWithPlaceholders);
            System.setProperty(CONFIG_FILE_PROPERTY, world.propertiesFile.toString());
            customPropertiesFileCreated = true;
        });

        Given("trends stats file $trendsStatsFile containing", (final String contents) -> {
        	originalTrendsStats = contents;
            world.trendsStatsFile = createTempFile("trendsStatsFile", ".json");
            write(world.trendsStatsFile, singletonList(contents));
        });

        Given("a non-existant properties file $propertiesFile", () -> {
            world.propertiesFile = createTempFile("propertiesFile", ".properties");
            world.propertiesFile.toFile().delete();
            System.setProperty(CONFIG_FILE_PROPERTY, world.propertiesFile.toString());
        });
        
        Then("the trends stats file has been updated", () -> {
        	final String updatedTrendsStats = lines(world.trendsStatsFile).collect(joining(lineSeparator()));
        	assertThat(updatedTrendsStats).isNotEqualToIgnoringWhitespace(originalTrendsStats);
        });

        After(scenario -> {
            if (defaultPropertiesFileCreated) {
                world.propertiesFile.toFile().delete();
                defaultPropertiesFileCreated = false;
            }
            if (customPropertiesFileCreated) {
                System.setProperty(CONFIG_FILE_PROPERTY, "");
                world.propertiesFile.toFile().delete();
            }
        });
    }

	protected void writePropertiesFile(final World world, final String contentsWithPlaceholders) throws IOException {
		final String contents = replaceTrendsStatsFilePlaceholder(contentsWithPlaceholders, world);
		write(world.propertiesFile, singletonList(contents));
	}

	protected String replaceTrendsStatsFilePlaceholder(String contents, final World world) {
		if(world.trendsStatsFile != null) {
		    return contents.replaceAll("\\$trendsStatsFile", world.trendsStatsFile.toString());
		}
		return contents;
	}

}