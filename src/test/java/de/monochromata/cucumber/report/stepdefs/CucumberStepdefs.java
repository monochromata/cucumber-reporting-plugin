package de.monochromata.cucumber.report.stepdefs;

import static java.util.Optional.ofNullable;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;

import cucumber.api.cli.Main;
import cucumber.api.java8.En;
import de.monochromata.cucumber.report.World;

public class CucumberStepdefs implements En {

    private String[] args;
    private byte exitCode;
    private String output;
    private Exception exception;

    public CucumberStepdefs(final World world) {
        Given("Cucumber options {string}", (final String args) -> {
            this.args = args
                    .replaceAll("\\$testOutputDir", toString(world.testOutputDir))
                    .replaceAll("\\$propertiesFile", toString(world.propertiesFile))
                    .replaceAll("\\$featureFile", toString(world.featureFile))
                    .split(" ");
        });

        When("Cucumber is run", () -> {
            final ByteArrayOutputStream systemOutBaos = new ByteArrayOutputStream();
            final ByteArrayOutputStream systemErrBaos = new ByteArrayOutputStream();
            final PrintStream oldSystemOut = System.out;
            final PrintStream oldSystemErr = System.err;
            try {
                System.setOut(new PrintStream(systemOutBaos));
                System.setErr(new PrintStream(systemErrBaos));
                exitCode = Main.run(args, Thread.currentThread().getContextClassLoader());
            } catch (final Exception e) {
                exception = e;
            } finally {
                System.setOut(oldSystemOut);
                System.setErr(oldSystemErr);
                output = systemOutBaos.toString();
            }
        });

        Then("Cucumber finishes successfully and output matches",
                (final String expectedOutput) -> {
                    assertThat(output).matches(expectedOutput);
                    assertThat(exitCode).isEqualTo((byte) 0);
                });

        Then("a {string} is thrown with message {string}",
                (final String exceptionTypeName, final String messageTemplate) -> {
                    final String message = messageTemplate.replaceAll("\\$propertiesFile",
                            world.propertiesFile.toString());
                    assertThat(exception).isInstanceOf(Class.forName(exceptionTypeName)).hasMessage(message);
                });
    }

    protected String toString(final Path path) {
        return ofNullable(path)
                .map(Path::toAbsolutePath)
                .map(Path::toString)
                .orElse("");
    }

}
