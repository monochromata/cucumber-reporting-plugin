package de.monochromata.cucumber.report.stepdefs;

import static java.util.Collections.singletonList;

import java.nio.file.Files;

import cucumber.api.java8.En;
import de.monochromata.cucumber.report.World;

public class FeatureStepdefs implements En {

    public FeatureStepdefs(final World world) {
        Given("Feature $featureFile", (final String contents) -> {
            world.featureFile = Files.createTempFile("Test", ".feature");
            world.featureFile.toFile().deleteOnExit();
            Files.write(world.featureFile, singletonList(contents));
        });
    }

}
