package de.monochromata.cucumber.report;

import static java.io.File.createTempFile;
import static java.util.Collections.singletonList;

import java.io.File;
import java.io.IOException;

import cucumber.api.Plugin;
import cucumber.api.event.EventListener;
import cucumber.api.event.EventPublisher;
import cucumber.api.event.TestRunFinished;
import cucumber.runtime.formatter.PluginFactory;
import de.monochromata.cucumber.report.config.ConfigurationFactory;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

public class PrettyReports implements Plugin, EventListener {

    private final File outputDir;
    private final File jsonFile;
    private final EventListener delegateJsonEventListener;

    public PrettyReports() throws Exception {
        this(new File("target" + File.separator + "cucumber"));
    }

    public PrettyReports(final File outputDir) throws Exception {
        this.outputDir = outputDir;
        jsonFile = createTempFileDeletedOnExit();
        delegateJsonEventListener = createJsonEventListener(jsonFile);
    }

    protected File createTempFileDeletedOnExit() throws IOException {
        final File jsonFile = createTempFile("cucumber", ".json");
        jsonFile.deleteOnExit();
        return jsonFile;
    }

    protected static EventListener createJsonEventListener(final File jsonFile) {
        return (EventListener) new PluginFactory().create("json:" + jsonFile.getAbsolutePath());
    }

    @Override
    public void setEventPublisher(final EventPublisher publisher) {
        delegateJsonEventListener.setEventPublisher(publisher);
        publisher.registerHandlerFor(TestRunFinished.class, this::generatePrettyReport);
    }

    protected void generatePrettyReport(final TestRunFinished event) {
        final Configuration configuration = ConfigurationFactory.getConfiguration(outputDir);
        new ReportBuilder(singletonList(jsonFile.getAbsolutePath()), configuration).generateReports();
    }
}
